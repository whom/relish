;;; flesh-mode.el --- Flesh -*- coding: utf-8; lexical-binding: t; -*-

;; Copyright © 2023, by Ava Affine
;; Author: Ava Affine (ava@sunnypup.io)
;; Version: 1
;; Keywords: languages

;; This file is not part of GNU Emacs.

;;; License:
;; You can redistribute this program and/or modify it under the terms of the GNU General Public License version 3.

;;; Code:

(setq flesh-keywords
      (let* (
             (r-control-flow '("if" "let" "circuit" "while" "assert" "exit"))
             (r-declaration  '("lambda" "q" "quote" "def" "get-doc" "set-doc" "set?"))
             (r-shell        '("pipe" "load-to-string" "l" "load" "load-with" "cd" "fg" "bg"))
             (r-list         '("car" "len" "cons" "cdr" "reverse" "dq" "pop" "list?"))
             (r-math         '("float" "sub" "mul" "inc" "dec" "div" "gte?" "int" "mod" "exp" "lt?" "gt?" "add" "lte?"))
             (r-strings      '("strlen" "substr?" "echo" "split" "input" "concat" "string"))
             (r-bool         '("toggle" "bool" "and" "eq?" "not" "or"))
             (r-file         '("read-file" "append-file" "write-file" "exists?"))
             (r-misc         '("call" "help" "env" "eval"))
             (r-userlib      '("reduce" "prepend" "add-path" "set" "map" "get-paths" "cond"))

             (r-control-flow-regexp (regexp-opt r-control-flow 'words))
             (r-declaration-regexp (regexp-opt r-declaration 'words))
             (r-shell-regexp (regexp-opt r-shell 'words))
             (r-list-regexp (regexp-opt r-list 'words))
             (r-math-regexp (regexp-opt r-math 'words))
             (r-strings-regexp (regexp-opt r-strings 'words))
             (r-bool-regexp (regexp-opt r-bool 'words))
             (r-file-regexp (regexp-opt r-file 'words))
             (r-misc-regexp (regexp-opt r-misc 'words))
             (r-userlib-regexp (regexp-opt r-userlib 'words)))

        `(
          (,r-control-flow-regexp . 'font-lock-type-face)
          (,r-declaration-regexp  . 'font-lock-type-face)
          (,r-misc-regexp         . 'font-lock-type-face)

          (,r-list-regexp         . 'font-lock-builtin-face)

          (,r-math-regexp         . 'font-lock-keyword-face)
          (,r-strings-regexp      . 'font-lock-keyword-face)
          (,r-bool-regexp         . 'font-lock-keyword-face)

          (,r-shell-regexp        . 'font-lock-constant-face)
          (,r-file-regexp         . 'font-lock-constant-face)

          (,r-userlib-regexp      . 'font-lock-function-name-face)
          )))

(setq flesh-mode-syntax-table
      (let ((table (make-syntax-table)))
        ;; string delims
        (modify-syntax-entry ?'  "\"" table)
        (modify-syntax-entry ?\" "\"" table)
        (modify-syntax-entry ?`  "\"" table)

        ;; comment delims
        (modify-syntax-entry ?\; "<" table)
        (modify-syntax-entry ?\n ">" table)

        table))

(define-derived-mode relish-mode
  text-mode
  "Flesh"
  :syntax-table flesh-mode-syntax-table
  (setq font-lock-defaults '((flesh-keywords))))

(add-to-list 'auto-mode-alist '("\\.f\\'" . relish-mode))
(add-to-list 'auto-mode-alist '(".fleshrc" . relish-mode))
(provide 'relish-mode)

;;; flesh-mode.el ends here
