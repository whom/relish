pub fn expand_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, String> {
    if let Ctr::Seg(_) = *ast.car {
        Ok(*ast.car.clone())
    } else {
        Err("non list passed to expand!".to_string())
    }
}
