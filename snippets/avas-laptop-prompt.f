#!/bin/flesh

;; Flesh: Flexible Shell
;; Copyright (C) 2021 Ava Affine
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; AVAS LAPTOP PROMPT
;; This file contains a shell prompt configuration that outputs current dir
;; username, and percentages for all batteries on the system.
;; -> requires CFG_FLESH_POSIX=true and userlib.

(def _batteries "paths to batteries powering system"
  (lambda ()
    (let ((power-srcs-dir "/sys/class/power_supply")
          (power-srcs
            (split (load-to-string find /sys/class/power_supply -printf "%p ")
                   " "))
          (pwr-list ())
          (pwr-iter (pop power-srcs)))
      (while (gt? (len pwr-iter) 1)
        (let ((src (car pwr-iter))
              (rem (cdr pwr-iter))
              (match (substr? src "BAT")))
          (if match 
              (set (q pwr-list) (cons pwr-list src))
              ())
          (set (q pwr-iter) (pop rem))))
      pwr-list)))

(def display-batteries "display battery capacity"
  (lambda ()
    (let ((bat-iter (pop (_batteries)))
          (display ""))
      (while (gt? (len bat-iter) 1)
        (let ((bat (car bat-iter))
              (rem (cdr bat-iter))
              (b-perc     (load-to-string head -c -1 (concat bat "/capacity")))
              (b-cap-raw  (load-to-string head -c -1 (concat bat "/energy_full")))
              (b-cap      (div (int b-cap-raw) 1000000)))
          (set (q display) (concat display "[" b-cap "Wh: " b-perc "%" "]"))
          (set (q bat-iter) (pop rem))))
    display)))

(def CFG_FLESH_R_PROMPT "display battery info"
  ()
  (display-batteries))

(def _fancy-cwd "prints (up to) last three segments of current path"
  ()
  (let ((cdir (load-to-string pwd))
        (dir-segs (split cdir "/"))
        (dir-iter (dq dir-segs))
        (i 0))
    (if (lte? (len dir-segs) 4)
      cdir
      (let ((final ""))
        (while (lt? i 3)
          (set (q final) (concat "/" (car dir-iter) final))
          (set (q dir-iter) (dq (cdr dir-iter)))
          (inc i))
        (concat "..." final)))))

(def in-a-git-repo?
"returns true or false depending on if currently in a git repo"
  () (eq? (load-to-string git rev-parse --is-inside-work-tree) "true"))

(def git-repo-is-dirty?
"returns true or false depending on if current dir is a dirty git repo"
  () (not (eq? (load-to-string git diff "--stat") "")))

(def git-status "returns '(git:<branch>{!,})' if dir is in a git repository"
  ()
  (if (in-a-git-repo?)
    (concat
      "(git:"
      (load-to-string git rev-parse --abbrev-ref HEAD)
      (if (git-repo-is-dirty?)
        "!"
        "")
      ")")
    ""))

(def CFG_FLESH_L_PROMPT "display user and dir (git info in future)"
 ()
 (concat
   "[" USER "]" "\t"
   (_fancy-cwd) "\t"
   (git-status) "\t"
   "(" (load-to-string date "+%H:%M") ")"
   "\n" ;; newline before delimiter
  ))
