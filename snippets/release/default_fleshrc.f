#!/usr/bin/env flesh

(set (q CFG_FLESH_POSIX) true)
(set (q CFG_FLESH_ENV)   true)

(def snippets        'flesh conf snippets' (concat HOME "/.flesh/"))
(def _user_libraries 'flesh support libs'
  ((concat snippets '/userlib.f')
   (concat snippets '/genbind.f')
   (concat snippets '/interactive-devel.f')
   ;; add your own scripts here
   ))

(def loadlibs        'load all flesh libs'
  (lambda ()
    (let ((lib-iter (pop _user_libraries)))
      (while (gt? (len lib-iter) 1)
        (let ((lib  (car lib-iter))
              (next (cdr lib-iter)))
            (call lib)
            (def lib-iter '' (pop next)))))))
;(loadlibs)

(def user-paths 'custom path entries'
  ((concat HOME "/bin")
   ;; add more here
   ))

;; add path if not already included
;; only you can stop $PATH pollution
(map
  (lambda (entry) (if (not (contains? (get-paths) entry))
                    (add-path entry)
                    ()))
  user-paths)

(def _fancy-cwd 'prints (up to) last three segments of current path'
  ()
  (let ((cdir (load-to-string pwd))
        (dir-segs (split cdir '/'))
        (dir-iter (dq dir-segs))
        (i 0))
    (if (lte? (len dir-segs) 4)
      cdir
      (let ((final ""))
        (while (lt? i 3)
          (set (q final) (concat "/" (car dir-iter) final))
          (set (q dir-iter) (dq (cdr dir-iter)))
          (inc i))
        (concat "..." final)))))

;; (def CFG_FLESH_CD_CB "directory based configs" ....... )
(def CFG_FLESH_L_PROMPT "left prompt"  ()
  (concat
    "[" USER "]" "\t"
    (_fancy-cwd)
    "\n" ;; newline before delimiter
    ))

(def CFG_FLESH_R_PROMPT "right prompt" ()
 (concat
   "(" (load-to-string date "+%H:%M") ")" ))

(def CFG_FLESH_PROMPT_DELIMITER "delimiter" ()
  "  -> ")
