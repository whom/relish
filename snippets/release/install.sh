#!/bin/sh

if [ "$USER" != "root" ]; then
    echo you will likely need root to place in /bin.
    echo comment out this test to continue without it
    exit
fi

if [ ! -n "$SUDO_USER" ]; then
    echo SUDO_USER not set, so I dont know where to install to
    exit
fi

libdir=/home/$SUDO_USER/.flesh/

if [ ! -d "$libdir" ]; then
    mkdir $libdir
fi

if [ -d "/home/$SUDO_USER/.emacs.d" ]; then
    mv flesh-mode.el /home/$SUDO_USER/.emacs.d/
    chown $SUDO_USER:$SUDO_USER /home/$SUDO_USER/.emacs.d/flesh-mode.el
fi

mv userlib.f $libdir
mv genbind.f $libdir
mv interactive-devel.f $libdir
mv default_fleshrc.f /home/$SUDO_USER/.fleshrc
chown -R $SUDO_USER:$SUDO_USER $libdir
chown $SUDO_USER:$SUDO_USER /home/$SUDO_USER/.fleshrc

mv flesh /bin/flesh
chmod +x /bin/flesh
