;; comment out to load USERLIB
;; (call (concat HOME "/.flesh/userlib.f"))

;; if you have userlib
;; (set CFG_FLESH_POSIX "1")
;; else
(def CFG_FLESH_POSIX
     (get-doc (q CFG_FLESH_POSIX))
     "1")

(echo "Welcome back to Flesh.")
(echo "Enjoy your computing.")
