#!/bin/flesh

(def __mood 'current mood' ':3')

(def CFG_FLESH_L_PROMPT 'mood left prompt'
() (concat "(" __mood ")" ))

(def CFG_FLESH_R_PROMPT 'mood right prompt'
() 'call set-mood to change prompt')

(def CFG_FLESH_PROMPT_DELIMITER 'mood prompt delim'
() '>')

(def set-mood 'set the little ascii face in your prompt'
(mood) (set (q __mood) mood))
