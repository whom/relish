#!/bin/flesh

;; Flesh: Flexible Shell
;; Copyright (C) 2021 Ava Affine
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; INTERACTIVE DEVELOPMENT
;; This file contains features that help you develop new features at the REPL
;; TODO: Need an is_symbol impl or some way to reflect

;; lib setup
(if (not (set? reduce))
  ((echo "need map. Include userlib.f")
   (exit -1)))

;; basic goals
;; (def get-tree-deps
;;   'Gets all global symbols referenced by the definition of a symbol.
;;    assumes symbol is passed in quoted. Essentially this function expects
;;    a tree.'
;;   (sym)
;;   (reduce (lambda (res elem) (cond ((sym? elem) ;; symbol case
;;                                       ()
;;                                    ((list? elem) ;; list case
;;                                       (extend results (get-tree-deps (eval elem)))))))
;;        sym))   ;; then return a stack of symbols

;; (def dump-tree
;;   'writes a symbol to a library file'
;;   (sym lib) ;; check if lib exists
;;   ())       ;; write to lib

;; (def dump-tree-with-deps
;;   'writes a symbol, with all its dependencies, to a file'
;;   (sym lib) ;; make a list of sym and all its deps
;;   ())       ;; iterate over that list and add new elements that are deps of iter
               ;; when a full iteration happens with no change, begin dumping all symbols into lib

;; stretch goals
;; (def diff-sym '' ())
;; (def diff-sym-with-deps '' ())
