/* relish: versatile lisp shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::eval::eval;
use crate::segment::{Seg, Ctr, Type};
use crate::vars::{VAR_TABLE, VTable, LIB_EXPORT};
use std::collections::HashMap;
use std::convert::TryInto;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref FUNC_TABLE: FTable<'static> = {
        let mut tab = FTable::new();
        tab.declare(LIB_EXPORT);
        tab
    };
}


pub struct FTable<'a> (HashMap<String, &'a Function>);

impl FTable<'_> {
    pub fn declare(&mut self, func: Box<Function>) -> Option<String> {
        if let Operation::External(ref fun) = &func.function {
            if let Args::Lazy(ref i) = func.args {
                if fun.arg_syms.len() != i.clone().try_into().unwrap() {
                    return Some(
                        "external function must have lazy args equal to declared arg_syms length"
                            .to_string(),
                    );
                }
            } else {
                return Some("external function must have lazy args".to_string());
            }
        }

        self.0.insert(func.name, func);
        None
    }

    pub fn get(&self, id: String) -> Option<&Box<Function>> {
        self.0.get(&id)
    }

    pub fn remove(&mut self, id: String) {
        self.0.remove(&id);
    }

    pub fn new() -> FTable<'static> {
        FTable{0: HashMap::<String, Box<Function>>::new()}
    }
}


// Standardized function signature for stdlib functions
//pub type InternalOperation = impl Fn(&Seg, &mut VTable, &mut FTable) -> Ctr;

#[derive(Debug)]
pub struct ExternalOperation<'a> {
    // Un-evaluated abstract syntax tree
    // TODO: Intermediate evaluation to simplify branches with no argument in them
    //       Simplified branches must not have side effects.
    // TODO: Apply Memoization?
    pub ast: Box<Seg<'a>>,
    // list of argument string tokens
    pub arg_syms: Vec<String>,
}

/* A stored function may either be a pointer to a function
 * or a syntax tree to eval with the arguments
 */
pub enum Operation<'a> {
    Internal(Box<dyn Fn(&'a Seg) -> Ctr<'a>>),
    External(ExternalOperation<'a>),
}

/* Function Args
 * If Lazy, is an integer denoting number of args
 * If Strict, is a list of type tags denoting argument type.
 */
pub enum Args {
    // signed: -1 denotes infinite args
    Lazy(i128),
    Strict(Vec<Type>),
}

impl Args {
    fn validate_inputs(&self, args: &Seg) -> Result<(), String> {
        match self {
            Args::Lazy(ref num) => {
                let called_arg_count = args.len() as i128;
                if *num == 0 {
                    if let Ctr::None = *args.car {
                        //pass
                    } else {
                        return Err("Expected 0 args. Got one or more.".to_string());
                    }
                } else if *num > -1 && (*num != called_arg_count) {
                    return Err(format!(
                        "Expected {} args. Got {}.",
                        num, called_arg_count
                    ));
                }
            }

            Args::Strict(ref arg_types) => {
                let mut idx: usize = 0;
                let passes = args.circuit(&mut |c: &Ctr| -> bool {
                    if idx >= arg_types.len() {
                        return false;
                    }
                    if let Ctr::None = c {
                        return false;
                    }
                    if arg_types[idx] == c.to_type() {
                        idx += 1;
                        return true;
                    }
                    return false;
                });

                if passes && idx < (arg_types.len() - 1) {
                    return Err(format!(
                        "{} too few arguments",
                        arg_types.len() - (idx + 1)
                    ));
                }

                if !passes {
                    if idx < (arg_types.len() - 1) {
                        return Err(format!(
                            "argument {} is of wrong type (expected {})",
                            idx + 1,
                            arg_types[idx].to_string()
                        ));
                    }
                    if idx == (arg_types.len() - 1) {
                        return Err("too many arguments".to_string());
                    }
                }
            }
        }

        Ok(())
    }
}

pub struct Function {
    pub function: Operation,
    pub name: String,
    pub args: Args,

    // dont fail on undefined symbol (passed to eval)
    pub loose_syms: bool,

    // dont evaluate args at all. leave that to the function
    pub eval_lazy: bool,
}

impl<'b, 'c> Function {
    /* call
     * routine is called by eval when a function call is detected
     */
    pub fn func_call(
        &self,
        args: &'b Seg<'b>,
    ) -> Result<Box<Ctr<'c>>, String> {
        // put args in simplest desired form
        let evaluated_args;
        match eval(args, self.loose_syms, self.eval_lazy) {
            Ok(arg_data) => {
                if let Ctr::Seg(ast) = *arg_data {
                    evaluated_args = &ast;
                } else {
                    return Err("Panicking: eval returned not a list for function args.".to_string());
                }
            }
            Err(s) => {
                return Err(format!(
                    "error evaluating args to {}: {}",
                    self.name, s
                ))
            }
        }

        if let Err(msg) = self.args.validate_inputs(evaluated_args) {
            return Err(format!("failure to call {}: {}", self.name, msg));
        }

        /* corecursive with eval.
         * essentially calls eval on each body in the function.
         * result of the final body is returned.
         */
        match &self.function {
            Operation::Internal(ref f) => return Ok(Box::new(f(evaluated_args))),
            Operation::External(ref f) => {
                let mut holding_table = VTable::new();

                // Prep var table for function execution
                for n in 0..f.arg_syms.len() {
                    let iter_arg = evaluated_args[n];
                    if let Some(val) = VAR_TABLE.get(f.arg_syms[n]) {
                        holding_table.insert(f.arg_syms[n].clone(), val);
                    }

                    VAR_TABLE.insert(f.arg_syms[n].clone(), Box::new(iter_arg));
                }

                // execute function
                let mut result: Box<Ctr>;
                let iterate = &*(f.ast);
                loop {
                    if let Ctr::Seg(ref data) = *iterate.car {
                        match eval(data, self.loose_syms, true) {
                            Ok(ctr) => result = ctr,
                            Err(e) => return Err(e),
                        }
                    } else {
                        panic!("function body not in standard form!")
                    }

                    match *iterate.cdr {
                        Ctr::Seg(ref next) => iterate = next,
                        Ctr::None => break,
                        _ => panic!("function body not in standard form!"),
                    }
                }

                // clear local vars and restore previous values
                for n in 0..f.arg_syms.len() {
                    VAR_TABLE.remove(f.arg_syms[n]);
                    if let Some(val) = holding_table.get(f.arg_syms[n]) {
                        VAR_TABLE.insert(f.arg_syms[n].clone(), val);
                    }
                }

                return Ok(result);
            }
        }
    }
}
