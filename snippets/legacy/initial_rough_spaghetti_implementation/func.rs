/* relish: versatile lisp shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::eval::eval;
use crate::segment::{circuit, list_idx, list_len, Ast, Ctr, Type};
use crate::vars::VTable;
use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::TryInto;
use std::rc::Rc;

pub type FTable = HashMap<String, Rc<RefCell<Function>>>;

// Standardized function signature for stdlib functions
//pub type InternalOperation = impl Fn(Ast, Rc<RefCell<VTable>>, Rc<RefCell<FTable>>) -> Ctr;
#[derive(Debug)]
pub struct ExternalOperation {
    // Un-evaluated abstract syntax tree
    // TODO: Intermediate evaluation to simplify branches with no argument in them
    //       Simplified branches must not have side effects.
    // TODO: Apply Memoization?
    pub ast: Ast,
    // list of argument string tokens
    pub arg_syms: Vec<String>,
}

/* A stored function may either be a pointer to a function
 * or a syntax tree to eval with the arguments
 */
pub enum Operation {
    Internal(Box<dyn Fn(Ast, Rc<RefCell<VTable>>, Rc<RefCell<FTable>>) -> Ctr>),
    External(ExternalOperation),
}

/* Function Args
 * If Lazy, is an integer denoting number of args
 * If Strict, is a list of type tags denoting argument type.
 */
pub enum Args {
    // signed: -1 denotes infinite args
    Lazy(i128),
    Strict(Vec<Type>),
}

// function which does not need args checked
pub struct Function {
    pub function: Operation,
    pub name: String,
    pub args: Args,

    // dont fail on undefined symbol (passed to eval)
    pub loose_syms: bool,

    // dont evaluate args at all. leave that to the function
    pub eval_lazy: bool,
}

/* call
 * routine is called by eval when a function call is detected
 */
pub fn func_call(
    function: Rc<RefCell<Function>>,
    args: Ast,
    vars: Rc<RefCell<VTable>>,
    funcs: Rc<RefCell<FTable>>,
) -> Result<Ctr, String> {
    let called_func = function.borrow();
    let mut n_args: Ast = args.clone();
    if !called_func.eval_lazy {
        match eval(args, vars.clone(), funcs.clone(), called_func.loose_syms) {
            Ok(rc_seg) => {
                if let Ctr::Seg(ast) = rc_seg {
                    n_args = ast
                } else {
                    return Err("Panicking: eval returned not a list for function args.".to_string());
                }
            }
            Err(s) => {
                return Err(format!(
                    "error evaluating args to {}: {}",
                    called_func.name, s
                ))
            }
        }
    }

    match &called_func.args {
        Args::Lazy(num) => {
            let called_arg_count = list_len(n_args.clone()) as i128;
            if *num == 0 {
                if let Ctr::None = n_args.clone().borrow().car {
                    //pass
                } else {
                    return Err(format!(
                        "expected 0 args in call to {}. Got one or more.",
                        called_func.name,
                    ));
                }
            } else if *num > -1 && (*num != called_arg_count) {
                return Err(format!(
                    "expected {} args in call to {}. Got {}.",
                    num, called_func.name, called_arg_count
                ));
            }
        }

        Args::Strict(arg_types) => {
            let mut idx: usize = 0;
            let passes = circuit(n_args.clone(), &mut |c: &Ctr| {
                if idx >= arg_types.len() {
                    return false;
                }

                if let Ctr::None = c {
                    return false;
                }

                let ret = arg_types[idx] == c.to_type();
                if ret {
                    idx += 1;
                }
                return ret;
            });

            if passes && idx < (arg_types.len() - 1) {
                return Err(format!(
                    "{} too little arguments in call to {}",
                    arg_types.len() - (idx + 1),
                    called_func.name
                ));
            }

            if !passes {
                if idx < (arg_types.len() - 1) {
                    return Err(format!(
                        "argument {} in call to {} is of wrong type (expected {})",
                        idx + 1,
                        called_func.name,
                        arg_types[idx].to_str()
                    ));
                }

                if idx == (arg_types.len() - 1) {
                    return Err(format!(
                        "too many arguments in call to {}",
                        called_func.name
                    ));
                }
            }
        }
    }

    match &called_func.function {
        Operation::Internal(f) => return Ok(f(n_args, vars, funcs)),
        Operation::External(f) => {
            for n in 0..f.arg_syms.len() {
                let iter_arg = list_idx(n_args.clone(), n as u128);

                vars.borrow_mut()
                    .insert(f.arg_syms[n].clone(), Rc::new(iter_arg));
            }

            let mut result: Ctr;
            let mut iterate = f.ast.clone();
            loop {
                if let Ctr::Seg(ast) = iterate.borrow().clone().car {
                    match eval(ast, vars.clone(), funcs.clone(), called_func.loose_syms) {
                        Ok(ctr) => match ctr {
                            Ctr::Seg(ast) => result = ast.borrow().clone().car,
                            _ => result = ctr,
                        },
                        Err(e) => return Err(e),
                    }
                } else {
                    panic!("function body not in standard form!")
                }

                match iterate.clone().borrow().clone().cdr {
                    Ctr::Seg(next) => iterate = next.clone(),
                    Ctr::None => break,
                    _ => panic!("function body not in standard form!"),
                }
            }
            for n in 0..f.arg_syms.len() {
                vars.borrow_mut().remove(&f.arg_syms[n].clone());
            }

            return Ok(result);
        }
    }
}

pub fn func_declare(ft: Rc<RefCell<FTable>>, f: Rc<RefCell<Function>>) -> Option<String> {
    let func = f.borrow();
    let name = func.name.clone();
    if let Operation::External(fun) = &func.function {
        if let Args::Lazy(i) = func.args {
            if fun.arg_syms.len() != i.try_into().unwrap() {
                return Some(
                    "external function must have lazy args equal to declared arg_syms length"
                        .to_string(),
                );
            }
        } else {
            return Some("external function must have lazy args".to_string());
        }
    }

    drop(func);
    ft.borrow_mut().insert(name, f);
    None
}
