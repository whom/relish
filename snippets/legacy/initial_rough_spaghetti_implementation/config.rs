/* relish: versatile lisp shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::eval::eval;
use crate::func::{func_declare, Args, FTable, Function, Operation};
use crate::lex::lex;
use crate::segment::{Ast, Ctr};
use crate::stl::get_stdlib;
use crate::vars::{define, VTable};
use std::cell::RefCell;
use std::fs;
use std::io::{self, Write};
use std::rc::Rc;

fn prompt_default_callback(_: Ast, _: Rc<RefCell<VTable>>, _: Rc<RefCell<FTable>>) -> Ctr {
    return Ctr::String("λ ".to_string());
}

pub fn configure(filename: String, vars: Rc<RefCell<VTable>>) -> Result<Rc<RefCell<FTable>>, String> {
    let funcs;

    define(
        vars.clone(),
        String::from("CFG_RELISH_POSIX"),
        Rc::new(Ctr::String(String::from("0"))),
    );
    define(
        vars.clone(),
        String::from("CFG_RELISH_ENV"),
        Rc::new(Ctr::String(String::from("1"))),
    );

    match get_stdlib(vars.clone()) {
        Ok(f) => funcs = f,
        Err(s) => {
            funcs = Rc::new(RefCell::new(FTable::new()));
            println!("Couldnt get stdlib: {}", s)
        },
    }

    match func_declare(
        funcs.clone(),
        Rc::new(RefCell::new(Function {
            name: String::from("CFG_RELISH_PROMPT"),
            loose_syms: false,
            eval_lazy: false,
            args: Args::Lazy(0),
            function: Operation::Internal(Box::new(prompt_default_callback)),
        })),
    ) {
        Some(e) => return Err(e),
        None => {},
    }

    match fs::read_to_string(filename.clone()) {
        Err(s) => {
            return Err(format!("Couldnt open configuration file: {}", s));
        }

        Ok(raw_config) => {
            let mut l = raw_config;
            l = "(".to_owned() + &l + ")";

            match lex(l) {
                Err(s) => {
                    return Err(format!("Error in configuration: {}", s));
                }

                Ok(config) => {
                    if let Err(errst) = eval(config, vars, funcs.clone(), false) {
                        return Err(format!("Error loading {}:  {}", filename.clone(), errst));
                    }
                }
            }
        },
    }

    return Ok(funcs);
}
