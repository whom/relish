/* relish: highly versatile lisp interpreter
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#![feature(derive_default_enum)]

mod append;
mod config;
mod eval;
mod func;
mod lex;
mod segment;
mod stl;
mod str;
mod vars;

pub mod ast {
    pub use crate::eval::eval;
    pub use crate::func::{
        func_call, func_declare, Args, ExternalOperation, FTable, Function, Operation,
    };
    pub use crate::lex::lex;
    pub use crate::segment::{Ctr, Seg, Type};
    pub use crate::vars::{define, VTable};
}

pub mod stdlib {
    pub use crate::append::get_append;
    pub use crate::stl::get_stdlib;
    pub use crate::str::{get_concat, get_echo};
    pub use crate::vars::get_export;
}

pub mod aux {
    pub use crate::config::configure;
}
