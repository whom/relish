/* relish: versatile lisp shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::func::{Args, FTable, Function, Operation};
use crate::segment::{circuit, list_append, list_idx, new_ast, Ast, Ctr};
use crate::vars::VTable;
use std::cell::RefCell;
use std::rc::Rc;

pub fn get_append() -> Function {
    return Function {
        name: String::from("append"),
        loose_syms: false,
        eval_lazy: false,
        args: Args::Lazy(-1),
        function: Operation::Internal(Box::new(
            |a: Ast, _b: Rc<RefCell<VTable>>, _c: Rc<RefCell<FTable>>| -> Ctr {
                let ptr = list_idx(a.clone(), 0);
                match ptr {
                    Ctr::Seg(ref c) => {
                        let acpy = a.clone();
                        match acpy.borrow().cdr.clone() {
                            Ctr::Seg(cn) => {
                                // append to list case
                                if !circuit(cn, &mut |arg: &Ctr| -> bool {
                                    list_append(c.clone(), arg.clone());
                                    return true;
                                }) {
                                    eprintln!(
                                        "get_append circuit loop should not have returned false"
                                    );
                                }
                            }
                            _ => {
                                eprintln!("get_append args somehow not in standard form");
                            }
                        }

                        return ptr;
                    }
                    _ => {
                        let n = new_ast(Ctr::None, Ctr::None);
                        if !circuit(a, &mut |arg: &Ctr| -> bool {
                            list_append(n.clone(), arg.clone());
                            return true;
                        }) {
                            eprintln!("get_append circuit loop should not have returned false");
                        }

                        return Ctr::Seg(n);
                    }
                }
            },
        )),
    };
}
