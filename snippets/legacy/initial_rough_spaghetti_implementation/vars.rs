/* relish: versatile lisp shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::eval::eval;
use crate::func::{Args, FTable, Function, Operation};
use crate::segment::{ast_to_string, Ast, Ctr};
use std::cell::RefCell;
use std::collections::HashMap;
use std::env;
use std::rc::Rc;
/* Mapping between a string token and a tree of Segments
 * The string token can be found in any Ctr::Symbol value
 * it is expected that the trees stored are already evaluated
 */
pub type VTable = HashMap<String, Rc<Ctr>>;

// WARNING: make sure var_tree is properly evaluated before storing
pub fn define(vt: Rc<RefCell<VTable>>, identifier: String, var_tree: Rc<Ctr>) {
    if let Some(rc_segment) = vt.borrow_mut().insert(identifier, var_tree) {
        drop(rc_segment);
    }
}

pub fn get_export(env_cfg: bool) -> Function {
    // This is the most hilarious way to work around the lifetime of env_cfg
    // TODO: figure out the RUSTEY way of doing this
    if env_cfg {
        return Function {
            name: String::from("export"),
            loose_syms: true,
            eval_lazy: true,
            args: Args::Lazy(2),
            function: Operation::Internal(Box::new(
                |a: Ast, b: Rc<RefCell<VTable>>, c: Rc<RefCell<FTable>>| -> Ctr {
                    export_callback(true, a, b, c)
                },
            )),
        };
    } else {
        return Function {
            name: String::from("export"),
            loose_syms: true,
            eval_lazy: true,
            args: Args::Lazy(2),
            function: Operation::Internal(Box::new(
                |a: Ast, b: Rc<RefCell<VTable>>, c: Rc<RefCell<FTable>>| -> Ctr {
                    export_callback(false, a, b, c)
                },
            )),
        };
    }
}

fn export_callback(env_cfg: bool, a: Ast, b: Rc<RefCell<VTable>>, c: Rc<RefCell<FTable>>) -> Ctr {
    let inner = a.borrow_mut();
    match &inner.car {
        Ctr::Symbol(identifier) => {
            match &inner.cdr {
                Ctr::Seg(tree) => match eval(tree.clone(), b.clone(), c.clone(), false) {
                    Ok(seg) => match seg {
                        Ctr::Seg(val) => {
                            define(b, identifier.to_string(), Rc::new(val.borrow().clone().car));
                            if env_cfg {
                                match val.borrow().clone().car {
                                    Ctr::Symbol(s) => env::set_var(identifier, s),
                                    Ctr::String(s) => env::set_var(identifier, s),
                                    Ctr::Integer(i) => env::set_var(identifier, format!("{}", i)),
                                    Ctr::Float(f) => env::set_var(identifier, format!("{}", f)),
                                    Ctr::Bool(b) => env::set_var(identifier, format!("{}", b)),
                                    Ctr::Seg(c) => env::set_var(identifier, ast_to_string(c)),
                                    Ctr::None => (),
                                }
                            }
                        }

                        _ => eprintln!("impossible args to export"),
                    },

                    Err(e) => eprintln!("couldnt eval symbol: {}", e),
                },

                Ctr::None => {
                    (*b).borrow_mut().remove(identifier);
                    if env_cfg {
                        // TODO: unset variable
                    }
                }

                _ => eprintln!("args not in standard form"),
            }
        }

        _ => eprintln!("first argument to export must be a symbol"),
    }

    return Ctr::None;
}
