/* relish: versatile lisp shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::func::{Args, FTable, Function, Operation};
use crate::segment::{ast_as_string, circuit, Ast, Ctr};
use crate::vars::VTable;
use std::cell::RefCell;
use std::rc::Rc;

// Current primitive is to use a get_NNNNN function defined for each library function which returns a not-previously-owned
// copy of the Function struct.
pub fn get_echo() -> Function {
    return Function {
        name: String::from("echo"),
        loose_syms: false,
        eval_lazy: false,
        args: Args::Lazy(-1),
        function: Operation::Internal(Box::new(
            |a: Ast, _b: Rc<RefCell<VTable>>, _c: Rc<RefCell<FTable>>| -> Ctr {
                let mut string = String::from("");
                if !circuit(a, &mut |arg: &Ctr| {
                    match arg {
                        // should be a thing here
                        Ctr::Symbol(_) => return false,
                        Ctr::String(s) => string.push_str(&s),
                        Ctr::Integer(i) => string.push_str(&i.to_string()),
                        Ctr::Float(f) => string.push_str(&f.to_string()),
                        Ctr::Bool(b) => string.push_str(&b.to_string()),
                        Ctr::Seg(c) => string.push_str(ast_as_string(c.clone(), true).as_str()),
                        Ctr::None => (),
                    }
                    println!("{}", string);
                    return true;
                }) {
                    eprintln!("circuit loop in echo should not have returned false")
                }
                return Ctr::None;
            },
        )),
    };
}

pub fn get_concat() -> Function {
    return Function {
        name: String::from("concat"),
        loose_syms: false,
        eval_lazy: false,
        args: Args::Lazy(-1),
        function: Operation::Internal(Box::new(
            |a: Ast, _b: Rc<RefCell<VTable>>, _c: Rc<RefCell<FTable>>| -> Ctr {
                let mut string = String::from("");
                if !circuit(a, &mut |arg: &Ctr| {
                    match arg {
                        // should be a thing here
                        Ctr::Symbol(_) => return false,
                        Ctr::String(s) => string.push_str(&s),
                        Ctr::Integer(i) => string.push_str(&i.to_string()),
                        Ctr::Float(f) => string.push_str(&f.to_string()),
                        Ctr::Bool(b) => string.push_str(&b.to_string()),
                        Ctr::Seg(c) => string.push_str(ast_as_string(c.clone(), true).as_str()),
                        Ctr::None => (),
                    }
                    return true;
                }) {
                    eprintln!("circuit loop in concat should not have returned false")
                }
                return Ctr::String(string);
            },
        )),
    };
}
