/* relish: versatile lisp shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::append::get_append;
use crate::func::{func_declare, FTable};
use crate::segment::Ctr;
use crate::str::{get_concat, get_echo};
use crate::control::{get_if};
use crate::vars::{get_export, VTable};
use std::cell::RefCell;
use std::rc::Rc;

pub fn get_stdlib(conf: Rc<RefCell<VTable>>) -> Result<Rc<RefCell<FTable>>, String> {
    let ft = Rc::new(RefCell::new(FTable::new()));
    if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(get_echo()))) {
        return Err(s);
    }
    if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(get_append()))) {
        return Err(s);
    }
    if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(get_concat()))) {
        return Err(s);
    }

    let mut cfg_env = true;
    match conf.borrow().get(&String::from("CFG_RELISH_ENV")) {
        None => {
            println!("CFG_RELISH_ENV not defined. defaulting to ON.")
        }
        Some(ctr) => match (**ctr).clone() {
            Ctr::String(ref s) => cfg_env = s.eq("0"),
            _ => {
                println!("Invalid value for CFG_RELISH_ENV. must be a string (0 or 1).");
                println!("Defaulting CFG_RELISH_ENV to ON");
            }
        },
    }

    if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(get_export(cfg_env)))) {
        return Err(s);
    }

    if let Some(s) = func_declare(ft.clone(), Rc::new(RefCell::new(get_if()))) {
        return Err(s);
    }

    return Ok(ft);
}
