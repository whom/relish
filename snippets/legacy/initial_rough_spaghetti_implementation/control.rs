/* relish: versatile lisp shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::append::get_append;
use crate::func::{func_declare, FTable, Ast};
use crate::segment::Ctr;
use crate::str::{get_concat, get_echo};
use crate::vars::{get_export, VTable};
use std::cell::RefCell;
use std::rc::Rc;

pub fn get_if() -> Function {
    return Function {
        name: String::from("if"),
        loose_syms: false,
        eval_lazy: true,
        args: Args::Lazy(-1),
        function: Operation::Internal(
            Box::new(|args: Ast, vars: Rc<RefCell<VTable>>, funcs: Rc<RefCell<FTable>>| -> Ctr {
                // Either 2 long or 3 long.
                // arg 1 must eval to a bool
                // then eval arg 2 or 3
            })
        ),
    };
}
