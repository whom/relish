mod append_lib_tests {
    use relish::ast::{ast_to_string, eval, lex, Ctr, FTable, VTable};
    use relish::stdlib::get_stdlib;
    use std::cell::RefCell;
    use std::rc::Rc;

    #[test]
    fn test_append_to_empty_list() {
        let document = "(append () 1 2 3)";
        let result = "(1 2 3)";
        let vt = Rc::new(RefCell::new(VTable::new()));
        let ft: Rc<RefCell<FTable>>;
        match get_stdlib(vt.clone()) {
            Ok(f) => ft = f,
            Err(s) => {
                ft = Rc::new(RefCell::new(FTable::new()));
                println!("Couldnt get stdlib: {}!", s);
                assert!(false)
            }
        }

        match lex(document.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}\n", document, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}\n", document, s);
                    assert!(false);
                }

                Ok(ctr) => match ctr {
                    Ctr::Symbol(_) => assert!(false),
                    Ctr::String(_) => assert!(false),
                    Ctr::Integer(_) => assert!(false),
                    Ctr::Float(_) => assert!(false),
                    Ctr::Bool(_) => assert!(false),
                    Ctr::Seg(s) => assert_eq!(ast_to_string(s), result),
                    Ctr::None => assert!(false),
                },
            },
        }
    }

    #[test]
    fn test_append_to_full_list() {
        let document = "(append (1 2) 3)";
        let result = "(1 2 3)";
        let vt = Rc::new(RefCell::new(VTable::new()));
        let ft: Rc<RefCell<FTable>>;
        match get_stdlib(vt.clone()) {
            Ok(f) => ft = f,
            Err(s) => {
                ft = Rc::new(RefCell::new(FTable::new()));
                println!("Couldnt get stdlib: {}!", s);
                assert!(false)
            }
        }

        match lex(document.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}\n", document, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}\n", document, s);
                    assert!(false);
                }

                Ok(ctr) => match ctr {
                    Ctr::Symbol(_) => assert!(false),
                    Ctr::String(_) => assert!(false),
                    Ctr::Integer(_) => assert!(false),
                    Ctr::Float(_) => assert!(false),
                    Ctr::Bool(_) => assert!(false),
                    Ctr::Seg(s) => assert_eq!(ast_to_string(s), result),
                    Ctr::None => assert!(false),
                },
            },
        }
    }

    #[test]
    fn test_mono_append() {
        let document = "(append)";
        let result = "()";
        let vt = Rc::new(RefCell::new(VTable::new()));
        let ft: Rc<RefCell<FTable>>;
        match get_stdlib(vt.clone()) {
            Ok(f) => ft = f,
            Err(s) => {
                ft = Rc::new(RefCell::new(FTable::new()));
                println!("Couldnt get stdlib: {}!", s);
                assert!(false)
            }
        }

        match lex(document.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}\n", document, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}\n", document, s);
                    assert!(false);
                }

                Ok(ctr) => match ctr {
                    Ctr::Symbol(_) => assert!(false),
                    Ctr::String(_) => assert!(false),
                    Ctr::Integer(_) => assert!(false),
                    Ctr::Float(_) => assert!(false),
                    Ctr::Bool(_) => assert!(false),
                    Ctr::Seg(s) => assert_eq!(ast_to_string(s), result),
                    Ctr::None => assert!(false),
                },
            },
        }
    }

    #[test]
    fn test_append_no_list() {
        let document = "(append 'test' 1 2 3)";
        let result = "('test' 1 2 3)";
        let vt = Rc::new(RefCell::new(VTable::new()));
        let ft: Rc<RefCell<FTable>>;
        match get_stdlib(vt.clone()) {
            Ok(f) => ft = f,
            Err(s) => {
                ft = Rc::new(RefCell::new(FTable::new()));
                println!("Couldnt get stdlib: {}!", s);
                assert!(false)
            }
        }

        match lex(document.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}\n", document, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}\n", document, s);
                    assert!(false);
                }

                Ok(ctr) => match ctr {
                    Ctr::Symbol(_) => assert!(false),
                    Ctr::String(_) => assert!(false),
                    Ctr::Integer(_) => assert!(false),
                    Ctr::Float(_) => assert!(false),
                    Ctr::Bool(_) => assert!(false),
                    Ctr::Seg(s) => assert_eq!(ast_to_string(s), result),
                    Ctr::None => assert!(false),
                },
            },
        }
    }
}
