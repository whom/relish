#!/bin/flesh

(def prepend
'takes a list and appends an element to the back of it.
 returns prepended list'
  (elem list)
  (reverse (append (reverse list) elem)))
