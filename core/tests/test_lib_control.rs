mod control_lib_tests {
    use flesh::ast::{eval, lex, SymTable};
    use flesh::stdlib::static_stdlib;

    #[test]
    fn test_assert_t() {
        let document = "(assert true)";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
    }

    #[test]
    fn test_assert_f() {
        let document = "(assert false)";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert!(eval(&lex(&document.to_string()).unwrap(), &mut syms).is_err())
    }

    #[test]
    fn test_if_first_case_singlet() {
        let document = "(if true 1 2)";
        let result = 1;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_if_second_case_singlet() {
        let document = "(if false 1 2)";
        let result = 2;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_complex_case_call() {
        let document = "(if true (cons () 1) 2)";
        let result = "(1)";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_let_multiphase_locals() {
        let document = "(let (
                          (temp \"1\")
                          (temp (cons () temp \"2\")))
                         temp)";
        let result = "(\"1\" \"2\")";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_let_def_escapes_locals() {
        let document1 = "(let (
                           (temp \"hello\")
                           (temp (concat temp \" \" \"world\")))
                          (def global \"\" temp))";
        let document2 = "global";
        let result = "(\"hello world\")";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        eval(&lex(&document1.to_string()).unwrap(), &mut syms).unwrap();
        assert_eq!(
            *eval(&lex(&document2.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_let_multibody_evals() {
        let document = "(let ((temp \"1\")) temp (cons () temp \"2\"))";
        let result = "(\"1\" \"2\")";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_let_multiphase_local_multibody_evals() {
        let document = "(let (
                          (temp \"1\")
                          (temp (cons () temp \"2\")))
                         (echo \"first body\")
                         (cons temp \"3\"))";

        let result = "(\"1\" \"2\" \"3\")";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_while_basic() {
        let switch_dec = "(def switch \"\" true)";
        // if prev is true, switch looped once and only once
        // else prev will have a problematic type
        let while_loop = "
            (while switch
              (def prev \"\" switch)
              (toggle switch)
              (if switch
                 (def \"\" prev)
                 ()))";
        let test_check = "prev";

        let switch_tree = lex(&switch_dec.to_string()).unwrap();
        let while_tree = lex(&while_loop.to_string()).unwrap();
        let check_tree = lex(&test_check.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&switch_tree, &mut syms).unwrap();
        eval(&while_tree, &mut syms).unwrap();
        eval(&check_tree, &mut syms).unwrap();
    }

    #[test]
    fn test_while_eval_cond() {
        let switch_dec = "(def switch \"\" true)";
        // if prev is true, switch looped once and only once
        // else prev will have a problematic type
        let while_loop = "
            (while (or switch switch)
              (def prev \"\" switch)
              (toggle switch)
              (if switch
                 (def \"\" prev)
                 ()))";
        let test_check = "prev";

        let switch_tree = lex(&switch_dec.to_string()).unwrap();
        let while_tree = lex(&while_loop.to_string()).unwrap();
        let check_tree = lex(&test_check.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&switch_tree, &mut syms).unwrap();
        eval(&while_tree, &mut syms).unwrap();
        eval(&check_tree, &mut syms).unwrap();
    }

    #[test]
    fn test_while_2_iter() {
        let additional = "(def sw1 \"\" true)";
        let switch_dec = "(def sw2 \"\" true)";
        // while should loop twice and define result
        let while_loop = "
            (while sw1
              (toggle sw2)
              (if (and sw1 sw2)
                 (def sw1 \"\" false)
                 (def result \"\" \"yay\")))";
        let test_check = "result";

        let another_tree = lex(&additional.to_string()).unwrap();
        let switch_tree = lex(&switch_dec.to_string()).unwrap();
        let while_tree = lex(&while_loop.to_string()).unwrap();
        let check_tree = lex(&test_check.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&another_tree, &mut syms).unwrap();
        eval(&switch_tree, &mut syms).unwrap();
        eval(&while_tree, &mut syms).unwrap();
        eval(&check_tree, &mut syms).unwrap();
    }

    #[test]
    fn test_circuit_basic() {
        let document = "(if (circuit true (and true true) 0 true) (def result \"\" \"passed\") ())";
        let test = "result";

        let doc_tree = lex(&document.to_string()).unwrap();
        let test_tree = lex(&test.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&doc_tree, &mut syms).unwrap();
        let res = eval(&test_tree, &mut syms);
        res.unwrap();
    }

    #[cfg(not(feature = "implicit-load"))]
    #[test]
    fn test_circuit_fail() {
        let document = "(if (circuit true (and false true) true) (def result \"\" \"passed\") ())";
        let test = "result";

        let doc_tree = lex(&document.to_string()).unwrap();
        let test_tree = lex(&test.to_string()).unwrap();

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&doc_tree, &mut syms).unwrap();
        assert_eq!(
            eval(&test_tree, &mut syms).err().unwrap().0.first().unwrap().message,
            "(is an undefined symbol)".to_string()
        );
    }
}
