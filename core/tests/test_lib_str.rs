mod str_lib_tests {
    use flesh::ast::{eval, lex, SymTable};
    use flesh::stdlib::static_stdlib;

    #[test]
    fn test_simple_concat() {
        let document = "(concat \"test\")";
        let result = "\"test\"";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_poly_concat() {
        let document = "(concat \"test\" 1 2 3)";
        let result = "\"test123\"";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_empty_concat() {
        let document = "(concat)";
        let result = "\"\"";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_str() {
        let document = "(strlen \"test\")";
        let result = 4;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_int() {
        let document = "(strlen 1000)";
        let result = 4;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_float() {
        let document = "(strlen 10.2)";
        let result = 4;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_seg() {
        let document = "(strlen (1 2 3))";
        let result = 7;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strlen_bool() {
        let document = "(strlen true)";
        let result = 4;
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strcast_i() {
        let document = "(string 4)";
        let result = "\"4\"";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_strcast_seg() {
        let document = "(string (1 2 3))";
        let result = "\"(1 2 3)\"";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_contains() {
        let document = "(substr? \"bigger\" \"ger\")";
        let result = "true";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_doesnt_contain() {
        let document = "(substr? \"smaller\" \"ger\")";
        let result = "false";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_split() {
        let document = "(split \"one.two.three\" \".\")";
        let result = "(\"one\" \"two\" \"three\")";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_split_big_delim() {
        let document = "(split \"one:d:two:d:three\" \":d:\")";
        let result = "(\"one\" \"two\" \"three\")";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_splitnt() {
        let document = "(split \"one.two.three\" \"-\")";
        let result = "(\"one.two.three\")";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_substr_valid() {
        let document = "(substr \"test\" 0 4)";
        let result = "\"test\"";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_substr_start_neg() {
        let document = "(substr \"test\" -1 3)";
        let result = "start index cannot be negative";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let doc_res= eval(&lex(&document.to_string()).unwrap(), &mut syms);
        if let Err(e) = doc_res {
            assert_eq!(
                e.0.first().unwrap().message,
                result.to_string(),
            );
        } else {
            assert!(false);
        }
    }

    #[test]
    fn test_substr_end_neg() {
        let document = "(substr \"test\" 1 -3)";
        let result = "end index cannot be negative";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let doc_res= eval(&lex(&document.to_string()).unwrap(), &mut syms);
        if let Err(e) = doc_res {
            assert_eq!(
                e.0.first().unwrap().message,
                result.to_string(),
            );
        } else {
            assert!(false);
        }
    }

    #[test]
    fn test_substr_start_out_of_bounds() {
        let document = "(substr \"test\" 5 3)";
        let result = "start index larger than source string";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let doc_res= eval(&lex(&document.to_string()).unwrap(), &mut syms);
        if let Err(e) = doc_res {
            assert_eq!(
                e.0.first().unwrap().message,
                result.to_string(),
            );
        } else {
            assert!(false);
        }
    }

    #[test]
    fn test_substr_end_out_of_bounds() {
        let document = "(substr \"test\" 1 5)";
        let result = "end index larger than source string";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let doc_res= eval(&lex(&document.to_string()).unwrap(), &mut syms);
        if let Err(e) = doc_res {
            assert_eq!(
                e.0.first().unwrap().message,
                result.to_string(),
            );
        } else {
            assert!(false);
        }
    }

    #[test]
    fn test_substr_index_order() {
        let document = "(substr \"test\" 3 2)";
        let result = "end index must be larger than start index";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let doc_res= eval(&lex(&document.to_string()).unwrap(), &mut syms);
        if let Err(e) = doc_res {
            assert_eq!(
                e.0.first().unwrap().message,
                result.to_string(),
            );
        } else {
            assert!(false);
        }
    }
}
