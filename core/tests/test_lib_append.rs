mod append_lib_tests {
    use flesh::ast::{eval, lex, SymTable};
    use flesh::stdlib::static_stdlib;

    #[test]
    fn test_cons_to_empty_list() {
        let document = "(cons () 1)";
        let result = "(1)";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_multi_cons_to_empty_list() {
        let document = "(cons () 1 \"two\" 3.4)";
        let result = "(1 \"two\" 3.4)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_cons_to_full_list() {
        let document = "(cons (1 2) 3)";
        let result = "(1 2 3)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_mono_cons() {
        let document = "(cons)";
        let result = "(<nil>)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_cons_no_list() {
        let document = "(cons \"test\" 1 2 3)";
        let result = "(\"test\" 1 2 3)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_len_empty() {
        let document = "(len ())";
        let result = "0";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_len_normal() {
        let document = "(len (1 2 3))";
        let result = "3";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_car_empty() {
        let document = "(car ())";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .err()
                .unwrap()
                .0
                .first()
                .unwrap()
                .message,
            "input is empty".to_string(),
        );
    }

    #[test]
    fn test_car_normal() {
        let document = "(car (1 2 3))";
        let result = "1";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_cdr_empty() {
        let document = "(cdr ())";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .err()
                .unwrap()
                .0
                .first()
                .unwrap()
                .message,
            "input is empty".to_string(),
        );
    }

    #[test]
    fn test_cdr_normal() {
        let document = "(cdr (1 2 3))";
        let result = "3";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_pop() {
        let document = "(def test \"\" (pop (1 2 3)))";
        let check1 = "(car test)";
        let result1 = "1";
        let check2 = "(cdr test)";
        let result2 = "(2 3)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
        let ch1 = lex(&check1.to_string()).unwrap();
        let ch2 = lex(&check2.to_string()).unwrap();

        assert_eq!(
            *eval(&ch1, &mut syms).unwrap().to_string(),
            result1.to_string(),
        );

        assert_eq!(
            *eval(&ch2, &mut syms).unwrap().to_string(),
            result2.to_string(),
        );
    }

    #[test]
    fn test_pop_mono() {
        let document = "(def test \"\" (pop (1)))";
        let check1 = "(car test)";
        let result1 = "1";
        let check2 = "(cdr test)";
        let result2 = "(<nil>)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
        let ch1 = lex(&check1.to_string()).unwrap();
        let ch2 = lex(&check2.to_string()).unwrap();

        assert_eq!(
            *eval(&ch1, &mut syms).unwrap().to_string(),
            result1.to_string(),
        );

        assert_eq!(
            *eval(&ch2, &mut syms).unwrap().to_string(),
            result2.to_string(),
        );
    }

    #[test]
    fn test_dq() {
        let document = "(def test \"\" (dq (1 2 3)))";
        let check1 = "(car test)";
        let result1 = "3";
        let check2 = "(cdr test)";
        let result2 = "(1 2)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
        let ch1 = lex(&check1.to_string()).unwrap();
        let ch2 = lex(&check2.to_string()).unwrap();

        assert_eq!(
            *eval(&ch1, &mut syms).unwrap().to_string(),
            result1.to_string(),
        );

        assert_eq!(
            *eval(&ch2, &mut syms).unwrap().to_string(),
            result2.to_string(),
        );
    }

    #[test]
    fn test_dq_mono() {
        let document = "(def test \"\" (dq (1)))";
        let check1 = "(car test)";
        let result1 = "1";
        let check2 = "(cdr test)";
        let result2 = "(<nil>)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
        let ch1 = lex(&check1.to_string()).unwrap();
        let ch2 = lex(&check2.to_string()).unwrap();

        assert_eq!(
            *eval(&ch1, &mut syms).unwrap().to_string(),
            result1.to_string(),
        );

        assert_eq!(
            *eval(&ch2, &mut syms).unwrap().to_string(),
            result2.to_string(),
        );
    }

    #[test]
    fn test_reverse() {
        let document = "(reverse (\"test\" 1 2 3))";
        let result = "(3 2 1 \"test\")";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_reverse_mono() {
        let document = "(reverse (\"test\"))";
        let result = "(\"test\")";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_reverse_nil() {
        let document = "(reverse ())";
        let result = "(<nil>)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_islist_t() {
        let document = "(list? ())";
        let result = "true";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_islist_f() {
        let document = "(list? 1223)";
        let result = "false";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }
}
