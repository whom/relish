mod math_lib_tests {
    use flesh::ast::{eval, lex, Ctr, SymTable};
    use flesh::stdlib::static_stdlib;

    #[test]
    fn test_add_chain() {
        let document = "(add 1 2 3 4)";
        let result = "10";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_add_chain_mixed() {
        let document = "(add 1 2.2 3 4)";
        let result = "10.2";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_mul_chain() {
        let document = "(mul 1 2 3 4)";
        let result = "24";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_sub_chain() {
        let document = "(sub 1 2.2 3 4)";
        let result = "-8.2";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_div() {
        let document = "(div 10 5)";
        let result = "2";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_float_to_int() {
        let document = "(int 10.5)";
        let result = "10";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_string_to_int() {
        let document = "(int \"10\")";
        let result = "10";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_int_to_float() {
        let document = "(float 10)";
        let result = "10";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_string_to_float() {
        let document = "(float \"10.3\")";
        let result = "10.3";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_ii_mod() {
        let document = "(def test \"\" (mod 7 3))";
        let check1 = "(car test)";
        let result1 = "2";
        let check2 = "(cdr test)";
        let result2 = "1";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let _ = *eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
        assert_eq!(
            *eval(&lex(&check1.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result1.to_string(),
        );
        assert_eq!(
            *eval(&lex(&check2.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result2.to_string(),
        );
    }

    #[test]
    fn test_if_mod() {
        let document = "(def test \"\" (mod 7 3.3))";
        let check1 = "(car test)";
        let result1 = "2";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let _ = *eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
        assert_eq!(
            *eval(&lex(&check1.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result1.to_string(),
        );
    }

    #[test]
    fn test_fi_mod() {
        let document = "(def test \"\" (mod 7.2 2))";
        let check1 = "(car test)";
        let result1 = "3";
        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let _ = *eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
        assert_eq!(
            *eval(&lex(&check1.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result1.to_string(),
        );
    }

    #[test]
    fn test_ff_mod() {
        let document = "(def test \"\" (mod 7.2 3.3))";
        let check1 = "(car test)";
        let result1 = "2";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        let _ = *eval(&lex(&document.to_string()).unwrap(), &mut syms).unwrap();
        assert_eq!(
            *eval(&lex(&check1.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result1.to_string(),
        );
    }

    #[test]
    fn test_ii_gt_t() {
        let document = "(gt? 4 3)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_ii_gt_f() {
        let document = "(gt? 2 3)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_if_gt_t() {
        let document = "(gt? 4 3.1)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_if_gt_f() {
        let document = "(gt? 3 3.1)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_fi_gt_t() {
        let document = "(gt? 4.1 4)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_fi_gt_f() {
        let document = "(gt? 2.1 3)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_ff_gt_t() {
        let document = "(gt? 3.2 3.1)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_ff_gt_f() {
        let document = "(gt? 3.1 3.2)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_ii_lt_f() {
        let document = "(lt? 4 3)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_ii_lt_t() {
        let document = "(lt? 2 3)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_if_lt_f() {
        let document = "(lt? 4 3.1)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_if_lt_t() {
        let document = "(lt? 3 3.1)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_fi_lt_f() {
        let document = "(lt? 4.1 4)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_fi_lt_t() {
        let document = "(lt? 2.1 3)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_ff_lt_f() {
        let document = "(lt? 3.2 3.1)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_ff_lt_ft() {
        let document = "(lt? 3.1 3.2)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_lte() {
        let document = "(lte? 3.2 3.1)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_lte_e() {
        let document = "(lte? 3.2 3.2)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_gte() {
        let document = "(gte? 3.1 3.2)";
        let result = false;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_gte_e() {
        let document = "(gte? 3.1 3.1)";
        let result = true;

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());
        assert_eq!(
            *eval(&lex(&document.to_string()).unwrap(), &mut syms)
                .unwrap()
                .to_string(),
            result.to_string(),
        );
    }

    #[test]
    fn test_inc() {
        let document = "(def tester \"\" 1)";
        let change = "(inc tester)";
        let check = "(tester)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        eval(&change_tree, &mut syms).unwrap();

        if let Ctr::Seg(ref s) = *eval(&check_tree, &mut syms).unwrap() {
            if let Ctr::Integer(ref b) = *s.car {
                assert_eq!(2, *b)
            } else {
                panic!()
            }
        } else {
            panic!()
        }

        eval(&change_tree, &mut syms).unwrap();
        if let Ctr::Seg(ref s) = *eval(&check_tree, &mut syms).unwrap() {
            if let Ctr::Integer(ref b) = *s.car {
                assert_eq!(3, *b)
            } else {
                panic!()
            }
        } else {
            panic!()
        }
    }

    #[test]
    fn test_inc_errors_dont_lose_vars() {
        let document = "(def tester \"\" \"oops\")";
        let change = "(inc tester)";
        let check = "(tester)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        if let Err(s) = eval(&change_tree, &mut syms) {
            assert_eq!(
                s.0.first().unwrap().message,
                "expected tester to be an integer".to_string()
            );
            let intermediate = *eval(&check_tree, &mut syms).unwrap();
            if let Ctr::Seg(ref s) = intermediate {
                assert_eq!(s.to_string(), "(\"oops\")".to_string());
            } else {
                eprintln!("did not expect: {}", intermediate);
                panic!()
            }
        } else {
            eprintln!("shouldn't have succeeded!");
            panic!()
        }
    }

    #[test]
    fn test_inc_errors_dont_lose_funcs() {
        let document = "(def tester \"\" (oops) oops)";
        let change = "(inc tester)";
        let check = "(tester \"1\")";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        if let Err(s) = eval(&change_tree, &mut syms) {
            assert_eq!(
                s.0.first().unwrap().message,
                "expected tester to be an integer".to_string()
            );
            if let Ctr::String(ref s) = *eval(&check_tree, &mut syms).unwrap() {
                assert_eq!(*s, "1".to_string());
            } else {
                panic!()
            }
        } else {
            eprintln!("shouldn't have succeeded!");
            panic!()
        }
    }

    #[test]
    fn test_dec() {
        let document = "(def tester \"\" 1)";
        let change = "(dec tester)";
        let check = "(tester)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        eval(&change_tree, &mut syms).unwrap();

        if let Ctr::Seg(ref s) = *eval(&check_tree, &mut syms).unwrap() {
            if let Ctr::Integer(ref b) = *s.car {
                assert_eq!(0, *b)
            } else {
                panic!()
            }
        } else {
            panic!()
        }

        eval(&change_tree, &mut syms).unwrap();
        if let Ctr::Seg(ref s) = *eval(&check_tree, &mut syms).unwrap() {
            if let Ctr::Integer(ref b) = *s.car {
                assert_eq!(-1, *b)
            } else {
                panic!()
            }
        } else {
            panic!()
        }
    }

    #[test]
    fn test_dec_errors_dont_lose_vars() {
        let document = "(def tester \"\" \"oops\")";
        let change = "(dec tester)";
        let check = "(tester)";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        if let Err(s) = eval(&change_tree, &mut syms) {
            assert_eq!(
                s.0.first().unwrap().message,
                "expected tester to be an integer".to_string()
            );
            let intermediate = *eval(&check_tree, &mut syms).unwrap();
            if let Ctr::Seg(ref s) = intermediate {
                assert_eq!(s.to_string(), "(\"oops\")".to_string());
            } else {
                eprintln!("did not expect: {}", intermediate);
                panic!()
            }
        } else {
            eprintln!("shouldn't have succeeded!");
            panic!()
        }
    }

    #[test]
    fn test_dec_errors_dont_lose_funcs() {
        let document = "(def tester \"\" (oops) oops)";
        let change = "(dec tester)";
        let check = "(tester \"1\")";

        let mut syms = SymTable::new();
        static_stdlib(&mut syms, |_: &String| (), || String::new());

        let doc_tree = lex(&document.to_string()).unwrap();
        let change_tree = lex(&change.to_string()).unwrap();
        let check_tree = lex(&check.to_string()).unwrap();

        eval(&doc_tree, &mut syms).unwrap();
        if let Err(s) = eval(&change_tree, &mut syms) {
            assert_eq!(
                s.0.first().unwrap().message,
                "expected tester to be an integer".to_string()
            );
            if let Ctr::String(ref s) = *eval(&check_tree, &mut syms).unwrap() {
                assert_eq!(*s, "1".to_string());
            } else {
                panic!()
            }
        } else {
            eprintln!("shouldn't have succeeded!");
            panic!()
        }
    }
}
