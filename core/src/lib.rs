/* Flesh: Flexible Shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#![cfg_attr(not(test), no_std)]

mod eval;
mod lex;
mod segment;
mod stl;
mod sym;
mod error;

#[macro_use]
extern crate alloc;

pub mod ast {
    pub use crate::eval::eval;
    pub use crate::lex::lex;
    pub use crate::segment::{Ctr, Seg, Type};
    pub use crate::sym::{Args, SymTable, Symbol, UserFn, ValueType};
    pub use crate::error::{Traceback, start_trace};
}

pub mod stdlib {
    pub use crate::stl::{
        static_stdlib,
        CFG_FILE_VNAME,
        decl::STORE_DOCSTRING,
        decl::store_callback,
    };
}
