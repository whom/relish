/* Flesh: Flexible Shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::sym::SymTable;
use alloc::string::String;

pub mod append;
pub mod boolean;
pub mod control;
pub mod decl;
pub mod math;
pub mod strings;

pub const CFG_FILE_VNAME: &str = "FLESH_CFG_FILE";

/// static_stdlib
/// inserts all stdlib functions that can be inserted without
/// any kind of further configuration data into a symtable
#[inline]
pub fn static_stdlib(syms: &mut SymTable, print: fn(&String), read: fn() -> String) {
    append::add_list_lib(syms);
    strings::add_string_lib(syms, print, read);
    decl::add_decl_lib_static(syms, print);
    control::add_control_lib(syms);
    boolean::add_bool_lib(syms);
    math::add_math_lib(syms);
}
