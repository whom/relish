/* Flesh: Flexible Shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use alloc::vec::Vec;
use alloc::string::String;
use core::fmt;
use core::convert;

#[derive(Debug, Clone)]
pub struct TraceItem {
    pub caller:  String,
    pub message: String,
}

#[derive(Debug, Clone)]
pub struct Traceback(pub Vec<TraceItem>);

#[inline]
pub fn start_trace(item: TraceItem) -> Traceback {
    Traceback::new().with_trace(item)
}

impl Traceback {
    pub fn new() -> Traceback {
        Traceback(Vec::new())
    }

    pub fn with_trace(mut self, item: TraceItem) -> Traceback {
        self.0.push(item);
        self
    }

    pub fn depth(&self) -> usize {
        self.0.len()
    }
}

impl fmt::Display for Traceback {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        _ = writeln!(f, "** ERROR TRACEBACK");
        for trace in self.0.iter().rev() {
            _ = writeln!(f, " > {}: {}", trace.caller, trace.message);
        }
        writeln!(f, "Please refactor forms and try again...")
    }
}


impl convert::From<Traceback> for String {
    fn from(arg: Traceback) -> Self {
        format!("{}", arg)
    }
}

impl convert::From<(&String, &str)> for TraceItem {
    fn from(value: (&String, &str)) -> Self {
        TraceItem {
            caller:  value.0.clone(),
            message: String::from(value.1),
        }
    }
}

impl convert::From<(&String, String)> for TraceItem {
    fn from(value: (&String, String)) -> Self {
        TraceItem {
            caller: value.0.clone(),
            message: value.1,
        }
    }
}

impl convert::From<(&str, String)> for TraceItem {
    fn from(value: (&str, String)) -> Self {
        TraceItem {
            caller: String::from(value.0),
            message: value.1,
        }
    }
}

impl convert::From<(&str, &str)> for TraceItem {
    fn from(value: (&str, &str)) -> Self {
        TraceItem {
            caller: String::from(value.0),
            message: String::from(value.1),
        }
    }
}
