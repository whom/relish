/* Flesh: Flexible Shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::segment::{Ctr, Seg, Type};
use crate::sym::{SymTable, Symbol, Args, ValueType};
use crate::error::{Traceback, start_trace};
use alloc::rc::Rc;
use alloc::boxed::Box;
use alloc::string::{String, ToString};


const CONS_DOCSTRING: &str = "traverses any number of arguments collecting them into a list.
If the first argument is a list, all other arguments are added sequentially to the end of the list contained in the first argument.";
fn cons_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, Traceback> {
    if let Ctr::Seg(ref s) = *ast.car {
        let mut temp = s.clone();
        if let Ctr::Seg(ref list) = *ast.cdr {
            list.circuit(&mut |c: &Ctr| -> bool {
                temp.append(Box::new(c.clone()));
                true
            });
        } else {
            temp.append(Box::new(*ast.cdr.clone()));
        }
        Ok(Ctr::Seg(temp))
    } else {
        let mut temp = Seg::new();
        let mut car_iter = &ast.car;
        let mut cdr_iter = &ast.cdr;
        loop {
            temp.append(car_iter.clone());
            if let Ctr::Seg(ref s) = **cdr_iter {
                car_iter = &s.car;
                cdr_iter = &s.cdr;
            } else {
                break;
            }
        }

        Ok(Ctr::Seg(temp))
    }
}

const LEN_DOCSTRING: &str = "Takes a single argument, expected to be a list.
Returns the length of said list.
Interpreter will panic if the length of the list is greater than the max value of a signed 128 bit integer";
fn len_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, Traceback> {
    if let Ctr::Seg(ref s) = *ast.car {
        if let Ctr::None = *s.car {
            Ok(Ctr::Integer(0))
        } else {
            Ok(Ctr::Integer(s.len() as i128))
        }
    } else {
        Err(start_trace(("len", "input is not a list").into()))
    }
}

const CAR_DOCSTRING: &str = "Takes a single argument, expected to be a list.
Returns a copy of the first value in that list.
If the list is empty, returns an err to avoid passing back a null value.";
fn car_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, Traceback> {
    if let Ctr::Seg(ref s) = *ast.car {
        if let Ctr::None = *s.car {
            Err(start_trace(("len", "input is empty").into()))
        } else {
            Ok(*s.car.clone())
        }
    } else {
        Err(start_trace(("len", "input is not a list").into()))
    }
}

const CDR_DOCSTRING: &str = "Takes a single argument, expected to be a list.
Returns a copy of the last value in that list.
If the list is empty, returns an err to avoid passing back a null value.";
fn cdr_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, Traceback> {
    if let Ctr::Seg(ref s) = *ast.car {
        let mut ret = &s.car;
        let mut iter = &s.cdr;
        while let Ctr::Seg(ref next) = **iter {
            ret = &next.car;
            iter = &next.cdr;
        }
        if let Ctr::None = **ret {
            Err(start_trace(("cdr", "input is empty").into()))
        } else {
            Ok(*ret.clone())
        }
    } else {
        Err(start_trace(("cdr", "input is not a list").into()))
    }
}

const POP_DOCSTRING: &str = "Takes a single argument, expected to be a list.
Returns a list containing the first element, and the rest of the elements.

Example: (pop (1 2 3)) -> (1 (2 3)).

The head can then be accessed by car and the rest can be accessed by cdr.";
fn pop_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, Traceback> {
    if let Ctr::Seg(ref s) = *ast.car {
        if s.is_empty() {
            return Err(start_trace(("pop", "input is empty").into()));
        }
        let mut ret = Seg::new();
        ret.append(s.car.clone());
        if let Ctr::Seg(ref s2) = *s.cdr {
            ret.append(Box::new(Ctr::Seg(s2.clone())));
        } else {
            ret.append(Box::new(Ctr::Seg(Seg::new())));
        }
        Ok(Ctr::Seg(ret))
    } else {
        Err(start_trace(("pop", "input is not a list").into()))
    }
}

const DEQUEUE_DOCSTRING: &str = "Takes a single argument, expected to be a list.
Returns a list containing the last element, and the rest of the elements.

Example: (dq (1 2 3)) -> (3 (1 2)).

The last element can then be accessed by car and the rest can be accessed by cdr.";
fn dequeue_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, Traceback> {
    if let Ctr::Seg(ref s) = *ast.car {
        if s.is_empty() {
            return Err(start_trace(("dequeue", "expected an input").into()));
        }
        let mut rest = Seg::new();
        let mut iter = s;
        while *iter.cdr != Ctr::None {
            rest.append(Box::new(*iter.car.clone()));
            if let Ctr::Seg(ref next) = *iter.cdr {
                iter = next;
            } else {
                break;
            }
        }

        let mut ret = Seg::new();
        match *iter.car {
            Ctr::Seg(ref s) => {
                ret.append(s.car.clone());
            }
            _ => ret.append(iter.car.clone()),
        }

        ret.append(Box::new(Ctr::Seg(rest)));

        Ok(Ctr::Seg(ret))
    } else {
        Err(start_trace(("dequeue", "input is not a list").into()))
    }
}

const REVERSE_DOCSTRING: &str = "Takes a single argument, expected to be a list.
Returns the same list, but in reverse order.";
fn reverse_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, Traceback> {
    if let Ctr::Seg(ref s) = *ast.car {
        let mut ret = Seg::new();
        s.circuit_reverse(&mut |arg: &Ctr| -> bool {
            ret.append(Box::new(arg.clone()));
            true
        });

        Ok(Ctr::Seg(ret))
    } else {
        Err(start_trace(("reverse", "input is not a list").into()))
    }
}

const ISLIST_DOCSTRING: &str = "takes a single argument, returns true if argument is a list";
fn islist_callback(ast: &Seg, _syms: &mut SymTable) -> Result<Ctr, Traceback> {
    if let Ctr::Seg(_) = *ast.car {
        Ok(Ctr::Bool(true))
    } else {
        Ok(Ctr::Bool(false))
    }
}

pub fn add_list_lib(syms: &mut SymTable) {
    syms.insert(
        "cons".to_string(),
        Symbol {
            name: String::from("cons"),
            args: Args::Infinite,
            conditional_branches: false,
            docs: CONS_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(cons_callback)),
            optimizable: true,
            ..Default::default()
        },
    );

    syms.insert(
        "len".to_string(),
        Symbol {
            name: String::from("len"),
            args: Args::Strict(vec![Type::Seg]),
            conditional_branches: false,
            docs: LEN_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(len_callback)),
            optimizable: true,
            ..Default::default()
        },
    );

    syms.insert(
        "car".to_string(),
        Symbol {
            name: String::from("car"),
            args: Args::Strict(vec![Type::Seg]),
            conditional_branches: false,
            docs: CAR_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(car_callback)),
            optimizable: true,
            ..Default::default()
        },
    );

    syms.insert(
        "cdr".to_string(),
        Symbol {
            name: String::from("cdr"),
            args: Args::Strict(vec![Type::Seg]),
            conditional_branches: false,
            docs: CDR_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(cdr_callback)),
            optimizable: true,
            ..Default::default()
        },
    );

    syms.insert(
        "pop".to_string(),
        Symbol {
            name: String::from("pop"),
            args: Args::Strict(vec![Type::Seg]),
            conditional_branches: false,
            docs: POP_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(pop_callback)),
            optimizable: true,
            ..Default::default()
        },
    );

    syms.insert(
        "dq".to_string(),
        Symbol {
            name: String::from("dequeue"),
            args: Args::Strict(vec![Type::Seg]),
            conditional_branches: false,
            docs: DEQUEUE_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(dequeue_callback)),
            optimizable: true,
            ..Default::default()
        },
    );

    syms.insert(
        "reverse".to_string(),
        Symbol {
            name: String::from("reverse"),
            args: Args::Strict(vec![Type::Seg]),
            conditional_branches: false,
            docs: REVERSE_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(reverse_callback)),
            optimizable: true,
            ..Default::default()
        },
    );

    syms.insert(
        "list?".to_string(),
        Symbol {
            name: String::from("list?"),
            args: Args::Lazy(1),
            conditional_branches: false,
            docs: ISLIST_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(islist_callback)),
            optimizable: true,
            ..Default::default()
        },
    );
}
