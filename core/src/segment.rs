/* Flesh: Flexible Shell
 * Copyright (C) 2021 Ava Affine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::sym::UserFn;
use alloc::fmt;
use alloc::string::{String, ToString};
use alloc::boxed::Box;
use core::convert;
use core::marker::PhantomData;
use core::ops::{Add, Div, Index, Mul, Sub};

// Container
#[derive(Debug, Default)]
pub enum Ctr {
    Symbol(String),
    String(String),
    Integer(i128),
    Float(f64),
    Bool(bool),
    Seg(Seg),
    Lambda(UserFn),
    #[default]
    None,
}

// Type of Container
#[derive(PartialEq, Clone)]
pub enum Type {
    Symbol,
    String,
    Integer,
    Float,
    Bool,
    Seg,
    Lambda,
    None,
}

/* Segment
 * Holds two Containers.
 * Basic building block for more complex data structures.
 * I was going to call it Cell and then I learned about
 * how important RefCells were in Rust
 */
#[derive(Debug, Default, PartialEq)]
pub struct Seg {
    /* "Contents of Address Register"
     *  Historical way of referring to the first value in a cell.
     */
    pub car: Box<Ctr>,

    /* "Contents of Decrement Register"
     *  Historical way of referring to the second value in a cell.
     */
    pub cdr: Box<Ctr>,

    /* Stupid hack that makes rust look foolish.
     * Needed to determine variance of lifetime.
     * How this is an acceptable solution I have
     * not a single clue.
     */
    _lifetime_variance_determinant: PhantomData<()>,
}

static NOTHING: Ctr = Ctr::None;

impl Ctr {
    pub fn to_type(&self) -> Type {
        match self {
            Ctr::Symbol(_s) => Type::Symbol,
            Ctr::String(_s) => Type::String,
            Ctr::Integer(_s) => Type::Integer,
            Ctr::Float(_s) => Type::Float,
            Ctr::Bool(_s) => Type::Bool,
            Ctr::Seg(_s) => Type::Seg,
            Ctr::Lambda(_s) => Type::Lambda,
            Ctr::None => Type::None,
        }
    }
}

impl Seg {
    /* recurs over tree assumed to be list in standard form
     * appends object to end of list
     */
    pub fn append(&mut self, obj: Box<Ctr>) {
        if let Ctr::None = &*(self.car) {
            self.car = obj;
            return;
        }

        if let Ctr::Seg(s) = &mut *(self.cdr) {
            s.append(obj);
            return;
        }

        if let Ctr::None = &mut *(self.cdr) {
            self.cdr = Box::new(Ctr::Seg(Seg::from_mono(obj)));
            // pray for memory lost to the void
        }
    }

    /* applies a function across a list in standard form
     * function must take a Ctr and return a bool
     * short circuits on the first false returned.
     * also returns false on a non standard form list
     */
    pub fn circuit<F: FnMut(&Ctr) -> bool>(&self, func: &mut F) -> bool {
        if func(&self.car) {
            match &*(self.cdr) {
                Ctr::None => true,
                Ctr::Seg(l) => l.circuit(func),
                _ => false,
            }
        } else {
            false
        }
    }

    /* applies a function across a list in standard form
     * recurs before applying function to go in reverse
     * function must take a Ctr and return a bool
     * short circuits on the first false returned.
     * also returns false on a non standard form list
     */
    pub fn circuit_reverse<F: FnMut(&Ctr) -> bool>(&self, func: &mut F) -> bool {
        match &*self.cdr {
            Ctr::None => func(&self.car),
            Ctr::Seg(l) => l.circuit_reverse(func) && func(&self.car),
            _ => false,
        }
    }

    pub fn from_mono(arg: Box<Ctr>) -> Seg {
        Seg {
            car: arg,
            cdr: Box::new(Ctr::None),
            _lifetime_variance_determinant: PhantomData,
        }
    }

    pub fn from(car: Box<Ctr>, cdr: Box<Ctr>) -> Seg {
        Seg {
            car,
            cdr,
            _lifetime_variance_determinant: PhantomData,
        }
    }

    /* recurs over ast assumed to be list in standard form
     * returns length
     */
    pub fn len(&self) -> u128 {
        let mut len = 0;
        self.circuit(&mut |_c: &Ctr| -> bool {
            len += 1;
            true
        });
        len
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn new() -> Seg {
        Seg {
            car: Box::new(Ctr::None),
            cdr: Box::new(Ctr::None),
            _lifetime_variance_determinant: PhantomData,
        }
    }
}

#[inline]
fn seg_to_string(s: &Seg, parens: bool) -> String {
    let mut string = String::new();
    if parens {
        string.push('(');
    }
    match *(s.car) {
        Ctr::None => string.push_str("<nil>"),
        _ => string.push_str(&s.car.to_string()),
    }
    string.push(' ');
    match &*(s.cdr) {
        Ctr::Seg(inner) => string.push_str(&seg_to_string(inner, false)),
        Ctr::None => {
            string.pop();
        }
        _ => string.push_str(&s.cdr.to_string()),
    }
    if parens {
        string.push(')');
    }

    string
}

impl Clone for Seg {
    fn clone(&self) -> Seg {
        Seg {
            car: self.car.clone(),
            cdr: self.cdr.clone(),
            _lifetime_variance_determinant: PhantomData,
        }
    }
}

impl Index<usize> for Seg {
    type Output = Ctr;

    fn index(&self, idx: usize) -> &Self::Output {
        if idx == 0 {
            return &self.car;
        }

        if let Ctr::Seg(ref s) = *self.cdr {
            return s.index(idx - 1);
        }

        &NOTHING
    }
}

impl Clone for Ctr {
    fn clone(&self) -> Ctr {
        match self {
            Ctr::Symbol(s) => Ctr::Symbol(s.clone()),
            Ctr::String(s) => Ctr::String(s.clone()),
            Ctr::Integer(s) => Ctr::Integer(*s),
            Ctr::Float(s) => Ctr::Float(*s),
            Ctr::Bool(s) => Ctr::Bool(*s),
            Ctr::Seg(s) => Ctr::Seg(s.clone()),
            Ctr::Lambda(s) => Ctr::Lambda(s.clone()),
            Ctr::None => Ctr::None,
        }
    }
}

impl fmt::Display for Ctr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Ctr::Symbol(s) => write!(f, "{}", s),
            Ctr::String(s) => write!(f, "\"{}\"", s),
            Ctr::Integer(s) => write!(f, "{}", s),
            Ctr::Float(s) => write!(f, "{}", s),
            Ctr::Bool(s) => {
                if *s {
                    write!(f, "true")
                } else {
                    write!(f, "false")
                }
            }
            Ctr::Seg(s) => write!(f, "{}", s),
            Ctr::Lambda(l) => write!(f, "{}", l),
            Ctr::None => Ok(()),
        }
    }
}

impl PartialEq for Ctr {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Ctr::Symbol(r), Ctr::Symbol(l)) => *r == *l,
            (Ctr::String(r), Ctr::String(l)) => *r == *l,
            (Ctr::Bool(r), Ctr::Bool(l)) => *r == *l,
            (Ctr::Seg(r), Ctr::Seg(l)) => *r == *l,
            (Ctr::None, Ctr::None) => true,
            (Ctr::Integer(r), Ctr::Integer(l)) => *r == *l,
            (Ctr::Float(r), Ctr::Float(l)) => *r == *l,
            (Ctr::Integer(r), Ctr::Float(l)) => *r < f64::MAX as i128 && *r as f64 == *l,
            (Ctr::Float(r), Ctr::Integer(l)) => *l < f64::MAX as i128 && *r == *l as f64,
            _ => false,
        }
    }
}

impl Add<Ctr> for Ctr {
    type Output = Ctr;

    fn add(self, rh: Ctr) -> Ctr {
        match self {
            Ctr::Float(lhn) => match rh {
                Ctr::Float(rhn) => Ctr::Float(lhn + rhn),
                Ctr::Integer(rhn) => Ctr::Float(lhn + rhn as f64),
                _ => unimplemented!("non-numeral on right hand side of add"),
            },

            Ctr::Integer(lhn) => match rh {
                Ctr::Float(rhn) => Ctr::Float(lhn as f64 + rhn),
                Ctr::Integer(rhn) => Ctr::Integer(lhn + rhn),
                _ => unimplemented!("non-numeral on right hand side of add"),
            },

            Ctr::String(lhs) => match rh {
                Ctr::String(rhs) => Ctr::String(lhs + &rhs),
                _ => unimplemented!("add to string only implemented for other strings"),
            },

            _ => {
                unimplemented!("datum does not support add")
            }
        }
    }
}

impl Sub<Ctr> for Ctr {
    type Output = Ctr;

    fn sub(self, rh: Ctr) -> Ctr {
        match self {
            Ctr::Float(lhn) => match rh {
                Ctr::Float(rhn) => Ctr::Float(lhn - rhn),
                Ctr::Integer(rhn) => Ctr::Float(lhn - rhn as f64),
                _ => unimplemented!("non-numeral on right hand side of sub"),
            },

            Ctr::Integer(lhn) => match rh {
                Ctr::Float(rhn) => Ctr::Float(lhn as f64 - rhn),
                Ctr::Integer(rhn) => Ctr::Integer(lhn - rhn),
                _ => unimplemented!("non-numeral on right hand side of sub"),
            },

            _ => {
                unimplemented!("datum does not support sub")
            }
        }
    }
}

impl Mul<Ctr> for Ctr {
    type Output = Ctr;

    fn mul(self, rh: Ctr) -> Ctr {
        match self {
            Ctr::Float(lhn) => match rh {
                Ctr::Float(rhn) => Ctr::Float(lhn * rhn),
                Ctr::Integer(rhn) => Ctr::Float(lhn * rhn as f64),
                _ => unimplemented!("non-numeral on right hand side of mul"),
            },

            Ctr::Integer(lhn) => match rh {
                Ctr::Float(rhn) => Ctr::Float(lhn as f64 * rhn),
                Ctr::Integer(rhn) => Ctr::Integer(lhn * rhn),
                _ => unimplemented!("non-numeral on right hand side of mul"),
            },

            _ => {
                unimplemented!("datum does not support mul")
            }
        }
    }
}

impl Div<Ctr> for Ctr {
    type Output = Ctr;

    fn div(self, rh: Ctr) -> Ctr {
        match self {
            Ctr::Float(lhn) => match rh {
                Ctr::Float(rhn) => Ctr::Float(lhn / rhn),
                Ctr::Integer(rhn) => Ctr::Float(lhn / rhn as f64),
                _ => unimplemented!("non-numeral on right hand side of div"),
            },

            Ctr::Integer(lhn) => match rh {
                Ctr::Float(rhn) => Ctr::Float(lhn as f64 / rhn),
                Ctr::Integer(rhn) => Ctr::Integer(lhn / rhn),
                _ => unimplemented!("non-numeral on right hand side of div"),
            },

            _ => {
                unimplemented!("datum does not support div")
            }
        }
    }
}

impl fmt::Display for Seg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", seg_to_string(self, true))
    }
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let ret: &str = match self {
            Type::Symbol => "symbol",
            Type::String => "string",
            Type::Integer => "integer",
            Type::Float => "float",
            Type::Bool => "bool",
            Type::Seg => "segment",
            Type::Lambda => "lambda",
            Type::None => "none",
        };

        write!(f, "{}", ret)
    }
}

impl convert::From<String> for Type {
    fn from(value: String) -> Self {
        match value.as_str() {
            "symbol" => Type::Symbol,
            "string" => Type::String,
            "integer" => Type::Integer,
            "float" => Type::Float,
            "bool" => Type::Bool,
            "segment" => Type::Seg,
            "lambda" => Type::Lambda,
            _ => Type::None,
        }
    }
}
