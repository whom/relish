/* Flesh: Flexible Shell
 * Copyright (C) 2021 Ava Hahn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::env;
use flesh::ast::{Ctr, Traceback, Seg, SymTable, Type, ValueType};
use flesh::stdlib::store_callback;

pub fn store_callback_with_env_integration(
    ast: &Seg,
    syms: &mut SymTable
) -> Result<Ctr, Traceback> {
    let name = store_callback(ast, syms)?;
    if let Ctr::String(n) = &name {
        match ast.len() {
            1 => {
                env::remove_var(n);
            },
            3 if syms.contains_key(&n) => {
                if let ValueType::VarForm(val) = &syms.get(&n).unwrap().value {
                    match val.to_type() {
                        Type::Lambda => {},
                        Type::Seg => {},
                        _ => {
                            let mut s = val.to_string();
                            // eat printed quotes on a string val
                            if let Ctr::String(tok) = &**val {
                                s = tok.to_string();
                            }
                            env::set_var(n.clone(), s);
                        }
                    }
                }
            },
            _ => {}
        }
    }

    Ok(name)
}
