/* Flesh: Flexible Shell
 * Copyright (C) 2021 Ava Hahn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use flesh::ast::{Traceback, Ctr, Symbol, Args, ValueType, Seg, SymTable, Type};
use crate::stl::{CONSOLE_XDIM_VNAME, FLESH_DEFAULT_CONS_WIDTH};
use std::rc::Rc;

pub fn add_window_lib_funcs(syms: &mut SymTable) {
    syms.insert(
        "env".to_string(),
        Symbol {
            name: String::from("env"),
            args: Args::None,
            conditional_branches: false,
            docs: ENV_DOCSTRING.to_string(),
            value: ValueType::Internal(Rc::new(env_callback)),
            ..Default::default()
        },
    );
}

const ENV_DOCSTRING: &str = "takes no arguments
prints out all available symbols and their associated values";
fn env_callback(_ast: &Seg, syms: &mut SymTable) -> Result<Ctr, Traceback> {
    // get width of current output
    let xdim: i128;
    if let Ctr::Integer(dim) = *syms
        .call_symbol(&CONSOLE_XDIM_VNAME.to_string(), &Seg::new(), true)
        .unwrap_or_else(|_: Traceback| Box::new(Ctr::None)) {
            xdim = dim;
        } else {
            println!("{} contains non integer value, defaulting to {}",
                     CONSOLE_XDIM_VNAME, FLESH_DEFAULT_CONS_WIDTH);
            xdim = FLESH_DEFAULT_CONS_WIDTH as i128;
        }

    let mut v_col_len = 0;
    let mut f_col_len = 0;
    let mut functions = vec![];
    let mut variables = vec![];
    for (name, val) in syms.iter() {
        if let ValueType::VarForm(l) = &val.value {
            let token: String = match l.to_type() {
                Type::Lambda => format!("{}: <lambda>", name),
                Type::Seg => format!("{}: <form>", name),
                _ => format!("{}: {}", name, val.value),
            };

            if token.len() > v_col_len && token.len() < xdim as usize {
                v_col_len = token.len();
            }

            variables.push(token);
        } else {
            if f_col_len < name.len() && name.len() < xdim as usize {
                f_col_len = name.len();
            }
            functions.push(name.clone());
        }
    }

    let mut n_v_cols = xdim / v_col_len as i128;
    // now decrement to make sure theres room for two spaces of padding
    while n_v_cols > 1 && xdim % (v_col_len as i128) < (2 * n_v_cols) {
        n_v_cols -= 1;
    }
    // again for functions
    let mut n_f_cols = xdim / f_col_len as i128;
    while n_f_cols > 1 && xdim & (f_col_len as i128) < (2 * n_f_cols) {
        n_f_cols -= 1;
    }

    let mut col_iter = 0;
    println!("VARIABLES:");
    for var in variables {
        print!("{:v_col_len$}", var);
        col_iter += 1;
        if col_iter % n_v_cols == 0 {
            println!();
        } else {
            print!("  ");
        }
    }
    println!("\nFUNCTIONS:");
    col_iter = 0;
    for func in functions {
        print!("{:f_col_len$}", func);
        col_iter += 1;
        if col_iter % n_f_cols == 0 {
            println!();
        } else {
            print!("  ");
        }
    }
    Ok(Ctr::None)
}
